#import <WebKit/WebKit.h>

@interface NSDate (EXTERNAL)
-(BOOL)_web_isToday;
@end

@interface HistoryTableViewController : UITableViewController
-(NSDate*)date;
@end

@interface History
+(id)sharedHistory;
-(id)itemAtIndex:(NSUInteger)index fromDate:(NSDate*)date;
-(NSUInteger)numberOfItemsFromDate:(NSDate*)date;
-(NSUInteger)_recentCount;
-(void)savePendingChanges;
-(WebHistory*)webHistory;
@end

@interface AddressView
-(BOOL)_completionsAreSearches;
-(UITextField*)_textFieldForEditing;
-(void)_updateSearchCompletions;
@end

@interface BrowserController
+(id)sharedBrowserController;
-(NSArray*)recentSearches;
-(void)saveRecentSearches:(NSArray*)searches;
@end

%hook HistoryTableViewController
%new
-(void)tableView:(UITableView*)view commitEditingStyle:(UITableViewCellEditingStyle)style forRowAtIndexPath:(NSIndexPath*)ipath {
  if(style!=UITableViewCellEditingStyleDelete){return;}
  History* history=[%c(History) sharedHistory];
  NSDate* date=self.date;
  NSUInteger index=ipath.row,rcount=date?0:history._recentCount;
  id item=[history itemAtIndex:index fromDate:date];
  if([item isKindOfClass:[NSDate class]]){
    NSUInteger nsub=[history numberOfItemsFromDate:item],i;
    NSMutableArray* items=[NSMutableArray arrayWithCapacity:nsub];
    for (i=0;i<nsub;i++){
      id sub=[history itemAtIndex:i fromDate:item];
      if([sub isKindOfClass:%c(WebHistoryItem)]){
        [items addObject:[history itemAtIndex:i fromDate:item]];
      }
    }
    [history.webHistory removeItems:items];
  }
  else if([item isKindOfClass:%c(WebHistoryItem)]){
    [history.webHistory removeItems:[NSArray arrayWithObject:item]];
  }
  else {return;}
  [history savePendingChanges];
  [view beginUpdates];
  if(index<rcount && rcount==history._recentCount){
    if([[history itemAtIndex:rcount fromDate:nil] _web_isToday]){
      [view insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath
       indexPathForRow:rcount-1 inSection:ipath.section]]
       withRowAnimation:UITableViewRowAnimationBottom];
    }
    else {
      [view reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath
       indexPathForRow:rcount inSection:ipath.section]]
       withRowAnimation:UITableViewRowAnimationFade];
    }
  }
  [view deleteRowsAtIndexPaths:[NSArray arrayWithObject:ipath]
   withRowAnimation:UITableViewRowAnimationLeft];
  [view endUpdates];
}
%end

%hook AddressView
%new
-(UITableViewCellEditingStyle)tableView:(UITableView*)view editingStyleForRowAtIndexPath:(NSIndexPath*)ipath {
  return (self._completionsAreSearches && !self._textFieldForEditing.text.length)?
   UITableViewCellEditingStyleDelete:UITableViewCellEditingStyleNone;
}
%new
-(void)tableView:(UITableView*)view commitEditingStyle:(UITableViewCellEditingStyle)style forRowAtIndexPath:(NSIndexPath*)ipath {
  BrowserController* bc=[%c(BrowserController) sharedBrowserController];
  NSMutableArray* searches=[NSMutableArray arrayWithArray:[bc recentSearches]];
  [searches removeObjectAtIndex:ipath.row];
  [bc saveRecentSearches:searches];
  [self _updateSearchCompletions];
}
%end
